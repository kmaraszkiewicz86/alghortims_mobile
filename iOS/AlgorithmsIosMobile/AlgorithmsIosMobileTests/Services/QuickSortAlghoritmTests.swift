//
//  QuickSortAlghoritmTests.swift
//  AlgorithmsIosMobileTests
//
//  Created by Krzysztof Maraszkiewicz on 25/09/2021.
//

import XCTest

class QuickSortAlghoritmTests: XCTestCase {

    private var serviceUnderTests : QuickSortAlghoritmService?
    
    override func setUpWithError() throws {
        serviceUnderTests = QuickSortAlghoritmService()
    }

    override func tearDownWithError() throws {
        //serviceUnderTests = nil
    }

    func testCheckIfSortedNumberAreCorect() {
        
        let result = serviceUnderTests!.sort([4,3,2,1,0,-4])
        
        XCTAssertEqual(result, [-4,0,1,2,3,4])
    }
}
