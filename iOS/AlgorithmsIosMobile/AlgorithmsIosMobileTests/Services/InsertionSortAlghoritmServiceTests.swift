//
//  InsertionSortAlghoritmServiceTests.swift
//  AlgorithmsIosMobile
//
//  Created by Krzysztof Maraszkiewicz on 25/09/2021.
//

import XCTest

class InsertionSortAlghoritmServiceTests: XCTestCase {

    private var serviceUnderTests : InsertionSortAlghoritmService?
    
    override func setUpWithError() throws {
        serviceUnderTests = InsertionSortAlghoritmService()
    }

    override func tearDownWithError() throws {
        //serviceUnderTests = nil
    }

    func testCheckIfSortedNumberAreCorect() {
        
        let result = serviceUnderTests!.sort([4,3,2,1,0,-4])
        
        XCTAssertEqual(result, [-4,0,1,2,3,4])
        
    }
}
