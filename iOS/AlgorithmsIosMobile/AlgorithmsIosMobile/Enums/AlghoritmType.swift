//
//  AlghoritmType.swift
//  AlgorithmsIosMobile
//
//  Created by Krzysztof Maraszkiewicz on 25/09/2021.
//

import Foundation

enum AlghoritmType : CustomStringConvertible {
    case none
    case quickSort
    case insertionSort
    
    var description : String {
        switch self {
            case .none : return "Brak"
            case .insertionSort: return "Sortowanie przez wstawianie"
            case .quickSort: return "Sortowanie szybkie"
        }
    }
}
