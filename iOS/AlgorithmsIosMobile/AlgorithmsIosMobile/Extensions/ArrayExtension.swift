//
//  ArrayExtension.swift
//  AlgorithmsIosMobile
//
//  Created by Krzysztof Maraszkiewicz on 25/09/2021.
//

import Foundation
import CloudKit

typealias IntType = (Int)

extension Array where Iterator.Element == IntType {
    func convertToString() -> String {
        let sortedNumberInStringArray : [String] = map{ String($0) }
        return sortedNumberInStringArray.joined(separator: "-")
    }
}
