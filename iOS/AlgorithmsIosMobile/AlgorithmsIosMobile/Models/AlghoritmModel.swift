//
//  AlghoritmModel.swift
//  AlgorithmsIosMobile
//
//  Created by USER on 20/09/2021.
//

import SwiftUI

class AlghoritmModel : ObservableObject {
    private var numbersToSort = [5, 8, 0, 9, 1, 0, 4, 7, 0, 4, 5, 2]
    
    @Published var type: AlghoritmType = .none
    
    @Published var numbersToSortInString: String = ""
    
    @Published var result: String = ""
    
    func sort() {
        
        self.numbersToSortInString = numbersToSort.convertToString()
        
        let service = AlghoritmFactory().getService(type: type)
        
        self.result = service.sort(self.numbersToSort).convertToString()
        
        let sortedNumberInStringArray : [String] = service.sort(self.numbersToSort).map{ String($0) }
        self.result = sortedNumberInStringArray.joined(separator: "-")
    }
}
