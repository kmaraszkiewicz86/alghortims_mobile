//
//  ContentView.swift
//  AlgorithmsIosMobile
//
//  Created by USER on 16/09/2021.
//

import SwiftUI
import CoreData

struct ContentView: View {
    
    @ObservedObject var alghoritmModel: AlghoritmModel
    
    var body: some View {
        VStack {
            
            Text("Sorting array of: \(alghoritmModel.numbersToSortInString)")
            
            Picker(selection: $alghoritmModel.type, label: Text("Alghoritm type"), content: {
                Text("None").tag(AlghoritmType.none)
                Text("Insertion sort").tag(AlghoritmType.insertionSort)
                Text("Quick Sort").tag(AlghoritmType.quickSort)
            })
            
            Text("\(alghoritmModel.type.description)")
            
            Button("Sort") {
                alghoritmModel.sort()
            }
            
            Text("\(alghoritmModel.result)")
        }
    }

}

private let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .medium
    return formatter
}()

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(alghoritmModel: AlghoritmModel())
    }
}
