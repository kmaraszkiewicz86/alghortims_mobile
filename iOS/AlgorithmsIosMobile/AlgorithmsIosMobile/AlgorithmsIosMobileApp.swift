//
//  AlgorithmsIosMobileApp.swift
//  AlgorithmsIosMobile
//
//  Created by USER on 16/09/2021.
//

import SwiftUI

@main
struct AlgorithmsIosMobileApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView(alghoritmModel: AlghoritmModel())
        }
    }
}
