//
//  AlghoritmFactory.swift
//  AlgorithmsIosMobile
//
//  Created by USER on 20/09/2021.
//

import SwiftUI

class AlghoritmFactory {
    
    func getService (type: AlghoritmType) -> SortAlghoritmService {
        
        switch type {
            case .insertionSort:
                return InsertionSortAlghoritmService()
            case .quickSort:
                return QuickSortAlghoritmService()
            default:
                return QuickSortAlghoritmService();
        }
    }
}
