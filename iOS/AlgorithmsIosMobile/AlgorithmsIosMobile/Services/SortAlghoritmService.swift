//
//  SortAlghoritmService.swift
//  AlgorithmsIosMobile
//
//  Created by Krzysztof Maraszkiewicz on 25/09/2021.
//

import Foundation

protocol SortAlghoritmService {
    
    func sort(_ numbers: [Int]) -> [Int]
}
