//
//  QuickSortAlghoritmService.swift
//  AlgorithmsIosMobile
//
//  Created by Krzysztof Maraszkiewicz on 25/09/2021.
//

import Foundation

class QuickSortAlghoritmService : SortAlghoritmService {
    func  sort(_ numbers: [Int]) -> [Int] {
        
        var numbersToSort = numbers
        
        sort(&numbersToSort, 0, numbersToSort.count - 1)
        
        return numbersToSort;
    }
    
    private func sort(_ numbers: inout [Int], _ left: Int, _ right: Int) {
        if (left >= right) {
            return
        }
        
        let pivot = numbers[right]
        var border = left - 1
        var index = left
        
        while (index < right) {
            if (pivot > numbers[index]) {
                border += 1
                if (border != index) {
                    swap(&numbers, border, index)
                }
            }
            
            index += 1
        }
        
        border += 1;
        
        if (border != right) {
            swap(&numbers, border, right)
        }
        
        sort(&numbers, left, border - 1)
        sort(&numbers, border + 1, right)
    }
    
    private func swap(_ numbers: inout [Int], _ index1: Int, _ index2: Int) {
        let tmp = numbers[index1]
        numbers[index1] = numbers[index2]
        numbers[index2] = tmp
    }
}
