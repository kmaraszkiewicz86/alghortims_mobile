//
//  InsertionSortAlghoritmService.swift
//  AlgorithmsIosMobile
//
//  Created by Krzysztof Maraszkiewicz on 25/09/2021.
//

import Foundation

class InsertionSortAlghoritmService : SortAlghoritmService {
    
    func sort(_ numbers: [Int]) -> [Int] {
        
        var numbersToSort = numbers
        
        for index in 1..<numbersToSort.count {
            let key = numbersToSort[index]
            var outerIndex = index - 1
            
            while (outerIndex >= 0 && numbersToSort[outerIndex] > key) {
                numbersToSort[outerIndex + 1] = numbersToSort[outerIndex]
                
                outerIndex -= 1
            }
            
            numbersToSort[outerIndex + 1] = key
        }
        
        return numbersToSort;
    }
}
